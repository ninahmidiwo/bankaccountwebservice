/**
 * 
 */
package com.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.test.dao.BankAccountDAO;

/**
 * @author peninah
 *
 */
public class BankAccountApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(BankAccountConfig.class, BankAccountDAO.class);
		context.refresh();
		
		BankAccountDAO bankAccountDAO = (BankAccountDAO)context.getBean("bankAccountDAO");
		bankAccountDAO.create("Test Account", "00000011", 0.00);
		bankAccountDAO.getBankAccountByAccountName("Test Account");
		context.close();
	}

}
