/**
 * 
 */
package com.test.pojo;

import java.util.Date;

/**
 * @author peninah
 *
 */
public class BankAccount {
	private Integer id;
	private String accountName;
	private String accountNumber;
	private Double balance;
	
	private Integer dailyWithdrawalCount;
	private Double currentCumulativeWithdrawalAmount;
	private String lastWithdrawalDate;
	
	private Integer dailyDepositCount;
	private Double currentCumulativeDepositAmount;
	private String lastDepositDate;
	
	public final Integer DAILY_WITHDRAWAL_COUNT_LIMIT = 3;
	public final Double DAILY_WITHDRAWAL_AMOUNT_LIMIT = 50.0;
	public final Double WITHDRAWAL_LIMIT_PER_TRANSACTION = 20.0;
	
	public final Integer DAILY_DEPOSIT_COUNT_LIMIT = 4;
	public final Double DAILY_DEPOSIT_AMOUNT_LIMIT = 150.0;
	public final Double DEPOSIT_LIMIT_PER_TRANSACTION = 40.0;

	/**
	 * 
	 */
	public BankAccount() {
		// TODO Auto-generated constructor stub
	}
	
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountName() {
		return accountName;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getBalance() {
		return balance;
	}

	public Double getCurrentCumulativeWithdrawalAmount() {
		return currentCumulativeWithdrawalAmount;
	}

	public void setCurrentCumulativeWithdrawalAmount(Double currentCumulativeWithdrawalAmount) {
		this.currentCumulativeWithdrawalAmount = currentCumulativeWithdrawalAmount;
	}

	public Integer getDailyWithdrawalCount() {
		return dailyWithdrawalCount;
	}

	public void setDailyWithdrawalCount(Integer dailyWithdrawalCount) {
		this.dailyWithdrawalCount = dailyWithdrawalCount;
	}

	public String getLastWithdrawalDate() {
		return lastWithdrawalDate;
	}

	public void setLastWithdrawalDate(String lastWithdrawalDate) {
		this.lastWithdrawalDate = lastWithdrawalDate;
	}

	public Integer getDailyDepositCount() {
		return dailyDepositCount;
	}

	public void setDailyDepositCount(Integer dailyDepositCount) {
		this.dailyDepositCount = dailyDepositCount;
	}

	public Double getCurrentCumulativeDepositAmount() {
		return currentCumulativeDepositAmount;
	}

	public void setCurrentCumulativeDepositAmount(Double currentCumulativeDepositAmount) {
		this.currentCumulativeDepositAmount = currentCumulativeDepositAmount;
	}

	public String getLastDepositDate() {
		return lastDepositDate;
	}

	public void setLastDepositDate(String lastDepositDate) {
		this.lastDepositDate = lastDepositDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
}
