/**
 * 
 */
package com.test.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.test.pojo.BankAccount;

/**
 * @author peninah
 *
 */
public class BankAccountMapper implements RowMapper<BankAccount> {

	/**
	 * 
	 */
	public BankAccount mapRow(ResultSet rs, int rowNum) throws SQLException {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setId(rs.getInt("id"));
		bankAccount.setAccountName(rs.getString("account_name"));
		bankAccount.setAccountNumber(rs.getString("account_number"));
		bankAccount.setBalance(rs.getDouble("balance"));
		bankAccount.setCurrentCumulativeDepositAmount(rs.getDouble("current_cumulative_deposit_amount"));
		bankAccount.setDailyDepositCount(rs.getInt("daily_deposit_count"));
		bankAccount.setLastDepositDate(rs.getString("last_deposit_date"));
		bankAccount.setCurrentCumulativeWithdrawalAmount(rs.getDouble("current_cumulative_withdrawal_amount"));
		bankAccount.setDailyWithdrawalCount(rs.getInt("daily_withdrawal_count"));
		bankAccount.setLastWithdrawalDate(rs.getString("last_withdrawal_date"));
		return bankAccount;
	}
}
