/**
 * 
 */
package com.test.beans;

/**
 * @author peninah
 *
 */
public class BankAccount {
	// Leaving out Bank and Bank Branch details
	public boolean createAccount(String accountName, String accountNumber) {
		System.out.println(accountName);
		System.out.println(accountNumber);
		System.out.println(0.00);
		return true;
	}
	
	public String getAccountByAccountName(String accountName) {
		System.out.println(accountName);
		return accountName;
	}	
	
	public BankAccount getAccountByAccountNumber(String accountNumber) {
		return null;
	}
	
	public boolean deposit(double amount) {
		return true;
	}
	
	public boolean withdraw(double amount) {
		return true;
	}
	
	public double balance() {
		return 0.0;
	}
}
