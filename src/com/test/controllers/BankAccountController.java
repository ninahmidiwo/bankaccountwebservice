/**
 * 
 */
package com.test.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.test.dao.BankAccountDAO;
import com.test.pojo.BankAccount;

/**
 * @author peninah
 *
 */

@RestController
@EnableWebMvc
public class BankAccountController {
	@Autowired
	BankAccountDAO bankAccountDAO;

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello() {
		return "Welcome to the Bank Account Web Service using Spring";
	}

	@RequestMapping(value = "/createBankAccount", method = RequestMethod.POST, produces = "application/JSON")
	public ResponseEntity<Void> createBankAccount(@RequestParam String accountName,
			@RequestParam String accountNumber) {
		bankAccountDAO.create(accountName, accountNumber, 0.00);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/getBankAccount", method = RequestMethod.GET, produces = "application/JSON")
	public ResponseEntity<String> getBankAccountByAccountName(@RequestParam String accountName) {
		BankAccount bankAccount = bankAccountDAO.getBankAccountByAccountName(accountName);
		String result = "Bank account name " + bankAccount.getAccountName() + " - account number "
				+ bankAccount.getAccountNumber();
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.POST, produces = "application/JSON")
	public ResponseEntity<String> deposit(@RequestParam String accountNumber, @RequestParam Double amount) {
		try {
			BankAccount bankAccount = bankAccountDAO.getBankAccountByAccountNumber(accountNumber);
			Double balance = bankAccount.getBalance();
			Integer depositCount = bankAccount.getDailyDepositCount();
			String lastDepositDate = bankAccount.getLastDepositDate();
			Double currentCumulativeDepositAmount = bankAccount.getCurrentCumulativeDepositAmount();
			String result = "";

			if (amount <= 0) {
				result = "Invalid amount. Amount must be greater that 0";
				return new ResponseEntity<String>(result, HttpStatus.BAD_REQUEST);
			}

			if (amount > bankAccount.DEPOSIT_LIMIT_PER_TRANSACTION) {
				result = "Amount above limit. Maximum amount per transaction " + "is $"
						+ bankAccount.DEPOSIT_LIMIT_PER_TRANSACTION;
				return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date today = new Date();
			if (lastDepositDate != null) {
				String depositDate = dateFormat.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastDepositDate));
				if (depositDate.equals(dateFormat.format(today))) {

					if (depositCount >= bankAccount.DAILY_DEPOSIT_COUNT_LIMIT) {
						result = "Number of deposits for today exhausted. Maximum times " + "is "
								+ bankAccount.DAILY_DEPOSIT_COUNT_LIMIT + " pers day";
						return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
					}

					Double currentAmount = amount + currentCumulativeDepositAmount;
					if (currentAmount > bankAccount.DAILY_DEPOSIT_AMOUNT_LIMIT) {
						result = "Daily deposit limit for today has been exhausted. Maximum limit " + "is $"
								+ bankAccount.DAILY_DEPOSIT_AMOUNT_LIMIT;
						return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);

					}
				}
				if (!depositDate.equals(dateFormat.format(new Date()))) {
					resetDailyDepositValues(accountNumber, 0, 0.00);
				}
			}

			bankAccount.setBalance(balance + amount);
			bankAccount.setDailyDepositCount(depositCount + 1);
			bankAccount.setLastDepositDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			bankAccount.setCurrentCumulativeDepositAmount(currentCumulativeDepositAmount + amount);
			bankAccountDAO.update(bankAccount);

			result = "Amount $" + amount + " successfully deposited from account. " + "New balance is $"
					+ (balance + amount);
			return new ResponseEntity<String>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.POST, produces = "application/JSON")
	public ResponseEntity<String> withdraw(@RequestParam String accountNumber, @RequestParam Double amount) {
		try {
			BankAccount bankAccount = bankAccountDAO.getBankAccountByAccountNumber(accountNumber);
			Double balance = bankAccount.getBalance();
			Integer withdrawalCount = bankAccount.getDailyWithdrawalCount();
			String lastWithdrawalDate = bankAccount.getLastWithdrawalDate();
			Double currentCumulativeWithdrawalAmount = bankAccount.getCurrentCumulativeWithdrawalAmount();
			String result = "";

			if (amount <= 0) {
				result = "Invalid amount. Amount must be greater that 0";
				return new ResponseEntity<String>(result, HttpStatus.BAD_REQUEST);
			}

			if (amount > bankAccount.WITHDRAWAL_LIMIT_PER_TRANSACTION) {
				result = "Amount above limit. Maximum amount per transaction " + "is $"
						+ bankAccount.WITHDRAWAL_LIMIT_PER_TRANSACTION;
				return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
			}

			if (amount > balance) {
				result = "Insufficient funds";
				return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date today = new Date();
			if (lastWithdrawalDate != null) {
				String withdrawalDate = dateFormat.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastWithdrawalDate));
				if (withdrawalDate.equals(dateFormat.format(today))) {
					if (withdrawalCount >= bankAccount.DAILY_WITHDRAWAL_COUNT_LIMIT) {
						result = "Number of withdrawals for today exhausted. Maximum times " + "is "
								+ bankAccount.DAILY_WITHDRAWAL_COUNT_LIMIT + " per day";
						return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
					}

					// If result is not empty, skip this check and just return the current error
					Double currentAmount = amount + currentCumulativeWithdrawalAmount;
					if (currentAmount > bankAccount.DAILY_WITHDRAWAL_AMOUNT_LIMIT && result.isEmpty()) {
						result = "Daily withdrawal limit for today has been exhausted. Maximum limit " + "is $"
								+ bankAccount.DAILY_WITHDRAWAL_AMOUNT_LIMIT;
						return new ResponseEntity<String>(result, HttpStatus.UNPROCESSABLE_ENTITY);
					}
				}

				if (!withdrawalDate.equals(dateFormat.format(new Date()))) {
					resetDailyWithdrawalValues(accountNumber, 0, 0.00);
				}
			}

			bankAccount.setBalance(balance - amount);
			bankAccount.setDailyWithdrawalCount(withdrawalCount + 1);
			bankAccount.setLastWithdrawalDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			bankAccount.setCurrentCumulativeWithdrawalAmount(currentCumulativeWithdrawalAmount + amount);
			bankAccountDAO.update(bankAccount);

			result = "Amount $" + amount + " successfully withdrawn from account. " + "New balance is $"
					+ (balance - amount);
			return new ResponseEntity<String>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/balance", method = RequestMethod.GET, produces = "application/JSON")
	public ResponseEntity<String> balance(@RequestParam String accountNumber) {
		BankAccount bankAccount = bankAccountDAO.getBankAccountByAccountNumber(accountNumber);
		String result = "";

		if (bankAccount == null) {
			result = "Account not found";
			return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
		} else {
			result = "Account balance is $" + bankAccount.getBalance();
			return new ResponseEntity<String>(result, HttpStatus.OK);
		}
	}

	private void resetDailyDepositValues(String accountNumber, Integer depositCount, Double depositAmount) {
		bankAccountDAO.updateDepositValues(accountNumber, depositCount, depositAmount);
	}

	private void resetDailyWithdrawalValues(String accountNumber, Integer withdrawalCount, Double withdrawalAmount) {
		bankAccountDAO.updateWithdrawalValues(accountNumber, withdrawalCount, withdrawalAmount);
	}

	public void setBankAccountDAO(BankAccountDAO bankAccountDAO) {
		this.bankAccountDAO = bankAccountDAO;
	}

}
