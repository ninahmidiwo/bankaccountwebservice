/**
 * 
 */
package com.test;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.test.beans.BankAccount;
import com.test.dao.BankAccountDAO;
import com.test.dao.BankAccountDAOImpl;

/**
 * @author peninah
 *
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.test")
public class BankAccountConfig {
	
	@Autowired
    private ApplicationContext applicationContext;
	
	@Bean
	@Scope("prototype")
	public BankAccount bankAccount() {
		return new BankAccount();
	}
	
    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/bank_account_web_service");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }
	
    private DatabasePopulator createDatabasePopulator() {
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
        databasePopulator.setContinueOnError(true);
        databasePopulator.addScript(new ClassPathResource("schema.sql"));
        return databasePopulator;
    }
    
    @Bean(name = "jdbcTemplate")
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(getDataSource());
        return jdbcTemplate;
    }
 
    @Bean(name = "bankAccountDAO")
    public BankAccountDAO bankAccountDAO(){
    	BankAccountDAO bankAccountDAO = new BankAccountDAOImpl();
    	bankAccountDAO.setJdbcTemplate(jdbcTemplate());
        return bankAccountDAO;
    }
 

}

