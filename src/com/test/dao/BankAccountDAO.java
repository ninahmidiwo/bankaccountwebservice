/**
 * 
 */
package com.test.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.test.pojo.BankAccount;

/**
 * @author peninah
 *
 */

public interface BankAccountDAO {
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate);

	public void create(String accountName, String accountNumber, double balance);

	public BankAccount getBankAccount(Integer id);

	public BankAccount getBankAccountByAccountName(String accountName);

	public BankAccount getBankAccountByAccountNumber(String accountNumber);

	public List<BankAccount> listBankAccounts();

	public void delete(Integer id);

	public void update(BankAccount bankAccount);
	
	public void updateDepositValues(String accountNumber, Integer depositCount, Double currentDepositAmount);
	
	public void updateWithdrawalValues(String accountNumber, Integer withdrawalCount, Double currentWithdrawalAmount);
}
