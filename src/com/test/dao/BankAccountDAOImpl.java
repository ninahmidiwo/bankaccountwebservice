/**
 * 
 */
package com.test.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.test.mapper.BankAccountMapper;
import com.test.pojo.BankAccount;

/**
 * @author peninah
 *
 */

@Repository
public class BankAccountDAOImpl implements BankAccountDAO {
	private JdbcTemplate jdbcTemplateObject;

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplateObject = jdbcTemplate;
	}

	public void create(String accountName, String accountNumber, double balance) {
		String SQL = "insert into bank_account (account_name, account_number, balance) values (?, ?, ?)";
		jdbcTemplateObject.update(SQL, accountName, accountNumber, balance);
		System.out.println("Created Account: " + accountName + " : " + accountNumber);
	}

	public BankAccount getBankAccount(Integer id) {
		try {
			String SQL = "select * from bank_account where id = ?";
			BankAccount bankAccount = (BankAccount) jdbcTemplateObject.queryForObject(SQL, new Object[] { id },
					new BankAccountMapper());
			return bankAccount;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public BankAccount getBankAccountByAccountName(String accountName) {
		try {
			String SQL = "select * from bank_account where account_name = ?";
			BankAccount bankAccount = jdbcTemplateObject.queryForObject(SQL, new Object[] { accountName },
					new BankAccountMapper());
			return bankAccount;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public BankAccount getBankAccountByAccountNumber(String accountNumber) {
		try {
			String SQL = "select * from bank_account where account_number = ?";
			BankAccount bankAccount = jdbcTemplateObject.queryForObject(SQL, new Object[] { accountNumber },
					new BankAccountMapper());
			return bankAccount;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public List<BankAccount> listBankAccounts() {
		try {
			String SQL = "select * from bank_account";
			List<BankAccount> bankAccounts = jdbcTemplateObject.query(SQL, new BankAccountMapper());
			return bankAccounts;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public void delete(Integer id) {
		String SQL = "delete from bank_account where id = ?";
		jdbcTemplateObject.update(SQL, id);
		System.out.println("Deleted Bank Account with ID = " + id);
	}

	public void update(BankAccount bankAccount) {
		String SQL = "update bank_account set balance = ?, daily_withdrawal_count = ?, "
				+ "last_withdrawal_date = ?, current_cumulative_withdrawal_amount = ?, "
				+ "daily_deposit_count = ?, last_deposit_date = ?, "
				+ "current_cumulative_deposit_amount = ? where account_number = ?";
		jdbcTemplateObject.update(SQL, bankAccount.getBalance(), bankAccount.getDailyWithdrawalCount(),
				bankAccount.getLastWithdrawalDate(), bankAccount.getCurrentCumulativeWithdrawalAmount(),
				bankAccount.getDailyDepositCount(), bankAccount.getLastDepositDate(),
				bankAccount.getCurrentCumulativeDepositAmount(), bankAccount.getAccountNumber());
		System.out.println("Updated Bank Account with ID = " + bankAccount.getId());
	}

	public void updateDepositValues(String accountNumber, Integer depositCount, Double currentDepositAmount) {
		String SQL = "update bank_account set daily_deposit_count = ?, "
				+ "current_cumulative_deposit_amount = ? where account_number = ?";
		jdbcTemplateObject.update(SQL, depositCount, currentDepositAmount, accountNumber);
		System.out.println("Updated Bank Account");
	}

	public void updateWithdrawalValues(String accountNumber, Integer withdrawalCount, Double currentWithdrawalAmount) {
		String SQL = "update bank_account set daily_withdrawal_count = ?, "
				+ "current_cumulative_withdrawal_amount = ? where account_number = ?";
		jdbcTemplateObject.update(SQL, withdrawalCount, currentWithdrawalAmount, accountNumber);
		System.out.println("Updated Bank Account");
	}
}
