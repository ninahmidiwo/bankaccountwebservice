CREATE DATABASE bank_account_web_service;

USE bank_account_web_service;

CREATE TABLE bank_account(
   id INT NOT NULL AUTO_INCREMENT,
   account_name VARCHAR(100) NOT NULL UNIQUE,
   account_number  VARCHAR(50) NOT NULL UNIQUE,
   balance DOUBLE(16,2) DEFAULT '0.00',
   currency VARCHAR(20) DEFAULT 'USD',
   daily_withdrawal_count INT DEFAULT 0,
   current_cumulative_withdrawal_amount DOUBLE(16,2) DEFAULT 0.00,
   last_withdrawal_date datetime,
   daily_deposit_count INT DEFAULT 0,
   current_cumulative_deposit_amount DOUBLE(16,2) DEFAULT 0.00,
   last_deposit_date datetime,
   PRIMARY KEY (id)
);