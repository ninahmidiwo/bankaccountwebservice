# TalaTest


Requirements
======================================================
1. Java
2. MySQL
3. Spring 5.1.6
4. Tomcat 8
5. JUnit
6. Hansen
7. Postman


Instructions
======================================================

1. Use file schema.sql to construct the DB
2. Deploy .war located in project root named BankAccountWebService.war to Tomcat


Endpoints
======================================================
1. Create bank account

	localhost:8080/BankAccountWebService/createBankAccount?accountName=A1&accountNumber=001
	
	Method POST
	
	Params: accountName
			 accountNumber
			 

2. Get bank account

	localhost:8080/BankAccountWebService/getBankAccount?accountName=A1
	
	Method GET
	
	Params: accountName
	
	Returns the accountNumber	
	
	
3. Balance

	localhost:8080/BankAccountWebService/balance?accountNumber=001
	
	Method GET
	
	Params: accountNumber	
	
	
4. Deposit

	localhost:8080/BankAccountWebService/deposit?accountNumber=001&amount=12.0
	
	Method POST
	
	Params: accountNumber	
			 amount 
			 
4. Withdraw

	localhost:8080/BankAccountWebService/withdraw?accountNumber=001&amount=5.0
	
	Method POST
	
	Params: accountNumber	
			 amount		
			 
			 
JUNIT Tests
===================================================================
Implemented in directory tests/ package com.test.junit
Test case is class TestBankAccountController
Disclaimer: I have not been able to run these due to conflicting jar files causing an initialization error
            Because of time constraints, I have commented out the class
            The class however has the test scenarios. HTTP endpoint however work fine			 	 
