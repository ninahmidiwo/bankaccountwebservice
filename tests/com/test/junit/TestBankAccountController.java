/**
 * 
 */
package com.test.junit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.test.controllers.BankAccountController;
import com.test.dao.BankAccountDAO;

/**
 * @author peninah
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {})
@WebAppConfiguration
public class TestBankAccountController {
//	 private MockMvc mockMvc;
//	
//	@Autowired
//	private WebApplicationContext wac	 
//	 
//	  @Before
//	  public void setup() {
//	         mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//	  }
//
//	@Test
//	public void createBankAccountTest() {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		params.add("accountName", Mockito.anyString());
//		params.add("accountNumber", Mockito.anyString());
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		MvcResult result;
//		try {
//			result = mockMvc.perform(requestBuilder).andReturn();
//			MockHttpServletResponse response = result.getResponse();
//			
//			Assert.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	
//	
//	@Test
//	public void getBankAccountTest() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		requestBuilder = MockMvcRequestBuilders.get(
//				"/BankAccountWebService/getBankAccount?accountName=" + accountName);
//
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//
//		System.out.println(result.getResponse());
//		String expected = "Bank account name " + accountName + " - account number " + accountNumber;
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
//	}
//	
//	/* BALANCE ENDPOINT TESTS*/
//	
//	@Test
//	public void balanceWithNonExistentBankAccountTest() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		requestBuilder = MockMvcRequestBuilders.get("/BankAccountWebService/balance?accountNumber=" + Mockito.anyString());
//
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//
//		String expected = "Account not found";
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void balanceWithValidBankAccountTest() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		requestBuilder = MockMvcRequestBuilders.get("/BankAccountWebService/balance?accountNumber=" + accountNumber);
//
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//
//		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
//	}
//	
//	/* DEPOSIT ENDPOINT TESTS*/
//	
//	@Test
//	public void depositWhenAmountLessThanOneTest() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "0.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/deposit").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Account not found";
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void depositWhenMaxDailyCountLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "1.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/deposit").params(params);
//		
//		mockMvc.perform(requestBuilder).andReturn();
//		mockMvc.perform(requestBuilder).andReturn();
//		mockMvc.perform(requestBuilder).andReturn();
//		mockMvc.perform(requestBuilder).andReturn();
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Number of deposits for today exhausted. Maximum times is 4 per day";
//		
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void depositWhenMaxDailyAmountLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "200.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/deposit").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		result = mockMvc.perform(requestBuilder).andReturn();
//        String expected = "Daily deposit limit for today has been exhausted. Maximum limit is $150";
//		
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void depositWhenMaxDailyTransactionLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "50.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/deposit").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Amount above limit. Maximum amount per transaction is $40";
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
//	/* WITHDRAWAL ENDPOINT TESTS*/
//	
//	@Test
//	public void withdrawWhenAmountLessThanOneTest() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "0.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/withdraw").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Account not found";
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void withdrawWhenMaxDailyCountLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "2.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/withdraw").params(params);
//		
//		mockMvc.perform(requestBuilder).andReturn();
//		mockMvc.perform(requestBuilder).andReturn();
//		mockMvc.perform(requestBuilder).andReturn();
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Number of withdrawals for today exhausted. Maximum times is 3 per day";
//		
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void withdrawWhenMaxDailyAmountLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "100.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/withdraw").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		result = mockMvc.perform(requestBuilder).andReturn();
//        String expected = "Daily withdrawal limit for today has been exhausted. Maximum limit is $50";
//		
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
//	@Test
//	public void withdrawWhenMaxDailyTransactionLimit() throws Exception {
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
//		String accountName = Mockito.anyString();
//		String accountNumber = Mockito.anyString();
//		params.add("accountName", accountName);
//		params.add("accountNumber", accountNumber);
//		
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
//				"/BankAccountWebService/createBankAccount").params(params);
//
//		params = new LinkedMultiValueMap<String, String>();
//		params.add("accountNumber", accountNumber);
//		params.add("amount", "30.0");
//		
//		requestBuilder = MockMvcRequestBuilders.post("/BankAccountWebService/withdraw").params(params);
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		result = mockMvc.perform(requestBuilder).andReturn();
//		String expected = "Amount above limit. Maximum amount per transaction is $20";
//
//		Assert.assertEquals(expected, result.getResponse().getContentAsString(), false);
//		Assert.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), result.getResponse().getStatus());
//	}
//	
}
